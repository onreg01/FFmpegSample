package com.gitlab.onreg01.ffmpegsample;

import android.app.Application;
import android.content.res.AssetManager;
import android.util.Log;

import com.github.hiteshsondhi88.libffmpeg.FFmpeg;
import com.github.hiteshsondhi88.libffmpeg.LoadBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegNotSupportedException;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class App extends Application {

    public final static String LOG = "LOG_FFMPEG_SAMPLE";
    public final static String FILE_NAME = "video.mp4";
    public final static String GIF_NAME = "gif_video.gif";
    public static File VIDEO_FILE;
    public static File GIF_FILE;

    private LoadBinaryResponseHandler loadBinaryResponseHandler = new LoadBinaryResponseHandler() {

        @Override
        public void onFailure() {
            super.onFailure();
            Log.e(LOG, "error during initialization");
        }

    };

    @Override
    public void onCreate() {
        super.onCreate();

        FFmpeg ffmpeg = FFmpeg.getInstance(this);
        try {
            ffmpeg.loadBinary(loadBinaryResponseHandler);
        } catch (FFmpegNotSupportedException e) {
            Log.e(LOG, e.getMessage());
        }

        VIDEO_FILE = new File(getExternalCacheDir(), FILE_NAME);
        GIF_FILE = new File(getExternalCacheDir(), GIF_NAME);

        extractAsset();
    }

    private void extractAsset() {
        if (!VIDEO_FILE.exists()) {
            AssetManager assetManager = getAssets();
            try {
                InputStream in = assetManager.open(FILE_NAME);
                FileOutputStream out = new FileOutputStream(VIDEO_FILE);

                byte[] buffer = new byte[1024];
                int read;
                while ((read = in.read(buffer)) != -1) {
                    out.write(buffer, 0, read);
                }

            } catch (IOException e) {
                Log.e(LOG, "empty video assets");
            }
        }
    }
}
