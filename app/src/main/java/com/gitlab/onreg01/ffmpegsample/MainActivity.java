package com.gitlab.onreg01.ffmpegsample;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;

import com.github.hiteshsondhi88.libffmpeg.ExecuteBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.FFmpeg;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegCommandAlreadyRunningException;

import java.io.File;

public class MainActivity extends AppCompatActivity {

    private ProgressBar progressBar;
    private Button button;

    private ExecuteBinaryResponseHandler responseHandler = new ExecuteBinaryResponseHandler() {

        @Override
        public void onSuccess(String message) {
            super.onSuccess(message);
            Log.d(App.LOG, message);
            openGif();
        }

        @Override
        public void onProgress(String message) {
            super.onProgress(message);
            Log.d(App.LOG, message);
        }

        @Override
        public void onFailure(String message) {
            super.onFailure(message);
            Log.d(App.LOG, message);

        }

        @Override
        public void onStart() {
            super.onStart();
            button.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        public void onFinish() {
            super.onFinish();
            button.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        progressBar = (ProgressBar) findViewById(R.id.progress);
        button = (Button) findViewById(R.id.btn_covnert);
    }

    public void convert(View view) {
        FFmpeg ffmpeg = FFmpeg.getInstance(this);
        try {
            String command = "-y "
                    + "-i "
                    + App.VIDEO_FILE.getAbsolutePath()
                    + " "
                    + App.GIF_FILE.getAbsolutePath();


            ffmpeg.execute(command.split(" "), responseHandler);
        } catch (FFmpegCommandAlreadyRunningException e) {
            Log.e(App.LOG, e.getMessage());
        }
    }

    private  void openGif(){
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        intent.setDataAndType(Uri.fromFile(App.GIF_FILE), "image/*");
        startActivity(intent);
    }
}
